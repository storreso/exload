const { Router } = require("../router");
const bcrypt = require("bcrypt");
const { addSession } = require("../sessions");

class UsersRoutes extends Router {
  constructor(app, database, collection) {
    super(app, database, collection);

    this.authOne = this.authOne.bind(this);
    this.createOne = this.createOne.bind(this);

    this.sessions = [];

    this.addRoute({
      path: "/users/auth-one",
      callback: this.authOne,
      type: "post",
      autoRes: false,
    });

    this.addRoute({
      path: "/users/create-one",
      callback: this.createOne,
      type: "post",
      autoRes: false,
    });

    this.serve();
  }

  createOne(req, res, output) {
    if (req.body) {
      const validFields = ["email", "password"];

      var error = false;

      validFields.forEach((key) => {
        if (!Object.keys(req.body).includes(key)) error = true;
      });

      if (!error) {
        const findEmail = this.collection.findOne(
          { email: req.body.email },
          (err, emailRes) => {
            if (err) res.json(output);
            else {
              if (!emailRes) {
                const fixedData = {
                  ...req.body,
                  password: bcrypt.hashSync(req.body.password, 10),
                };

                this.collection.insertOne(fixedData, (err, resolve) => {
                  if (err) {
                    output.message = "Invalid insert";
                    res.json(output);
                  } else {
                    if (resolve.result.ok) {
                      output.message = "Valid insert.";
                      output.next = true;
                      addSession(req.body.email);
                    } else {
                      output.messae = "Server error.";
                    }
                  }

                  res.json(output);
                });
              } else {
                output.message = "Invalid email";
                res.json(output);
              }
            }
          }
        );
      } else res.json(output);
    } else res.json(output);
  }

  authOne(req, res, output) {
    if (req.body) {
      if (req.body.email && req.body.password) {
        this.collection.findOne({ email: req.body.email }, (err, resolve) => {
          if (err) output.message = "Server error.";
          else {
            if (
              resolve &&
              bcrypt.compareSync(req.body.password, resolve.password)
            ) {
              output.message = "Valid email and password";
              output.next = true;

              addSession(req.body.email);
            } else output.message = "Invalid email or password";
          }

          res.json(output);
        });
      } else res.json(output);
    } else res.json(output);
  }
}

module.exports = {
  UsersRoutes,
};
