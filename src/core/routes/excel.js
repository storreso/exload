const { Router } = require("../router");
const fileType = require("file-type");
const xlsx = require("xlsx");
const { validateSession, getSessions } = require("../sessions");
class ExcelRoutes extends Router {
  constructor(app, database, collection) {
    super(app, database, collection);

    this.uploadFile = this.uploadFile.bind(this);
    this.getAll = this.getAll.bind(this);

    this.addRoute({
      path: "/excel/upload",
      callback: this.uploadFile,
      type: "post",
      autoRes: false,
    });

    this.addRoute({
      path: "/excel/get-all",
      callback: this.getAll,
      type: "get",
      autoRes: false,
    });

    this.serve();
  }

  async getAll(req, res, output) {
    this.collection.find({}).toArray((err, resolve) => {
      if (err) output.message = "Server error.";
      else {
        output.next = true;
        output.message = "Valid find all";
        output.data["all"] = resolve;
      }

      res.json(output);
    });
  }

  async uploadFile(req, res, output) {
    if (req.query.email && validateSession(req.query.email)) {
      const now = new Date();

      const fileBuffer = await new Promise((res, rej) => {
        let dataParts = [Buffer.alloc(0)];
        req.on("data", (d) => dataParts.push(d));
        req.on("error", rej);
        req.on("end", () => res(Buffer.concat(dataParts)));
      });

      const type = await fileType.fromBuffer(fileBuffer);

      if (["xlsx"].includes(type.ext)) {
        const xl = xlsx.read(fileBuffer, { type: "buffer" });
        const sheet = xl.Sheets["Sheet1"];
        const keys = Object.keys(sheet);

        var columns = [];
        var data = {};
        var json = [];

        keys.forEach((key) => {
          if (!key.includes("!")) {
            if (key.length === 2 && key.split("")[1] === "1") {
              columns.push(sheet[key]);
            } else {
              const sub = key
                .split("")
                .splice(1, key.split("").length)
                .join("");
              if (!Object.keys(data).includes(sub)) data[sub] = [];
              data[sub].push(sheet[key]);
            }
          }
        });

        /**
         * Only save document field to improve database size
         * Dont save all keys with undefined values
         */

        Object.keys(data).forEach((row) => {
          if (Array.isArray(data[row])) {
            var fixed = { created: now };

            data[row].forEach((data, index) => {
              fixed[columns[index].v] = data.v ? data.v : undefined;
            });

            json.push(fixed);
          }
        });

        this.collection.insertMany(json, (err, resolve) => {
          if (err) {
            output.message = "Server error";
            console.log(err);
          } else {
            output.message = "valid upload";
            output.next = true;
          }

          res.json(output);
        });
      }
    } else {
      output.message = "Invalid session.";
      res.json(output);
    }
  }
}

module.exports = {
  ExcelRoutes,
};
