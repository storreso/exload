class Sessions {
  constructor() {
    this.addSession = this.addSession.bind(this);
    this.getSessions = this.getSessions.bind(this);
    this.validateSession = this.validateSession.bind(this);

    this.sessions = [];
  }

  addSession(email) {
    this.sessions.push(email);
  }

  getSessions() {
    return this.sessions;
  }

  validateSession(email) {
    return this.sessions.findIndex((session) => session === email) !== -1
      ? true
      : false;
  }
}

const sessions = new Sessions();

module.exports = {
  getSessions: sessions.getSessions,
  validateSession: sessions.validateSession,
  addSession: sessions.addSession,
};
