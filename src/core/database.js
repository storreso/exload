const mongo = require("mongodb").MongoClient;

class DBController {
  constructor(config) {
    this.config = config;
    this.url = this.createUrl();
    this.connection = null;
  }

  async connect() {
    this.connection = await new Promise((res, rej) => {
      try {
        mongo.connect(this.url, { useUnifiedTopology: true }, (err, db) => {
          if (err) rej(null);
          else {
            console.log("[DATABASE] Connected to database.");
            res(db);
          }
        });
      } catch (error) {
        rej(null);
      }
    });

    this.db = this.connection.db(this.config.name);
  }

  isConnected() {
    if (this.connection !== null) return true;
    else return false;
  }

  getConnection() {
    return this.db;
  }

  createUrl() {
    return (
      "mongodb://" +
      (this.config.host ? this.config.host : "localhost") +
      ":" +
      (this.config.port ? this.config.port : "27017")
    );
  }
}

module.exports = {
  DBController,
};
