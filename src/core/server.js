const express = require("express");

const { UsersRoutes } = require("./routes/users");
const { ExcelRoutes } = require("./routes/excel");

class ServerController {
  constructor(config, database) {
    this.config = config;
    this.database = database;

    this.app = express();

    this.app.use("/css", express.static(process.cwd() + "/src/views/css"));
    this.app.use("/js", express.static(process.cwd() + "/src/views/js"));

    this.app.get("/", (req, res) => {
      res.sendFile(process.cwd() + "/src/views/index.html");
    });

    this.app.use(express.json());

    this.init();
  }

  async init() {
    await this.database.connect();

    if (this.database.isConnected()) {
      this.users = new UsersRoutes(
        this.app,
        this.database.getConnection(),
        "users"
      );

      this.excel = new ExcelRoutes(
        this.app,
        this.database.getConnection(),
        "excel"
      );

      this.app.listen(this.config.port ? this.config.port : 3000, () => {
        console.log("[SERVER] Listening in " + this.config.port + ".");
      });
    }
  }
}

module.exports = {
  ServerController,
};
