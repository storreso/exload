class Router {
  constructor(app, database, collectionName) {
    this.app = app;
    this.database = database;
    this.collectionName = collectionName;
    this.table = null;
    this.collection = null;
    this.serving = false;

    this.routes = [];
  }

  async serve() {
    if (await this.validateCollection()) {
      await this.deployRoutes();
      this.serving = true;
    }
  }

  async validateCollection() {
    this.collection = await new Promise((res, rej) => {
      this.database.createCollection(this.collectionName, (err, collection) => {
        if (err) {
          if (err.code === 48) {
            res(this.database.collection(this.collectionName));
          } else rej(null);
        } else res(collection);
      });
    });

    return this.collection;
  }

  async deployRoutes() {
    if (this.routes.length > 0) {
      this.routes.forEach((route) => {
        this.app[route.type](route.path, async (req, res) => {
          const output = {
            message: "Invalid request",
            next: false,
            status: 200,
            data: {},
          };

          await route.callback(req, res, output);

          if (route.autoRes) {
            res.json(output);
          }
        });
      });
    }
  }

  addRoute(route) {
    this.routes.push(route);
  }
}

module.exports = {
  Router,
};
