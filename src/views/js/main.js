const form = document.getElementById("ctform");
const error = document.getElementById("cterror");
const uerror = document.getElementById("ctuloaderr");
const uploadcontainer = document.getElementById("ctupload");
const uploadform = document.getElementById("ctuploadform");
const table = document.getElementById("cttable");
const thead = document.querySelector("#cttable thead");
const tbody = document.querySelector("#cttable tbody");

const acceptedExts = ["xlsx"];

const updateTable = async () => {
  const request = await fetch("http://localhost:3000/excel/get-all");
  const response = await request.json();
  const columns = [];

  thead.innerHTML = "";
  tbody.innerHTML = "";

  if (response.next) {
    const trhead = document.createElement("tr");

    thead.appendChild(trhead);

    response.data["all"].forEach((row) => {
      const keys = Object.keys(row);

      keys.forEach((key) => {
        if (!columns.includes(key)) {
          const thead = document.createElement("th");
          thead.innerHTML = key;
          columns.push(key);
          trhead.appendChild(thead);
        }
      });

      const trbody = document.createElement("tr");

      columns.forEach((column) => {
        const thbody = document.createElement("th");

        if (Object.keys(row).includes(column)) {
          thbody.innerHTML = row[column];
        } else thbody.innerHTML = "";

        trbody.appendChild(thbody);
      });

      tbody.appendChild(trbody);
    });
  }
};

var login = {
  val: false,
  email: "",
};

updateTable();

form.addEventListener("submit", async (e) => {
  e.preventDefault();

  const data = {
    email: document.getElementById("emailinput").value,
    password: document.getElementById("passinput").value,
  };

  if (data.email === "") {
    error.innerHTML = "Invalid email";
    return;
  }

  if (data.password === "") {
    error.innerHTML = "Invalid password";
    return;
  }

  error.innerHTML = "";

  const request = await fetch("http://localhost:3000/users/auth-one", {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const response = await request.json();

  if (response.next) {
    error.innerHTML = response.message;
    console.log(uploadcontainer);
    uploadcontainer.classList.add("active");

    login.val = true;
    login.email = data.email;
  } else error.innerHTML = response.message;
});

uploadform.addEventListener("submit", async (e) => {
  e.preventDefault();

  if (login.email) {
    const files = document.getElementById("uploadinput");

    if (files.files.length > 0) {
      const request = await fetch("/excel/upload?email=" + login.email, {
        method: "POST",
        body: files.files[0],
        headers: {
          "Content-Type": files.files[0].type,
        },
      });

      const response = await request.json();

      if (response.next) {
        uerror.innerHTML = "Success upload.";

        updateTable();
      } else uerror.innerHTML = "Upload error.";
    } else uerror.innerHTML = "Invalid files.";
  } else uerror.innerHTML = "You need login.";
});
