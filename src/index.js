const { DBController } = require("./core/database");
const { ServerController } = require("./core/server");

const dbConfig = {
  host: "localhost",
  name: "exload",
  port: 27017,
};

const serverConfig = { port: 3000 };

const database = new DBController(dbConfig);
const server = new ServerController(serverConfig, database);
